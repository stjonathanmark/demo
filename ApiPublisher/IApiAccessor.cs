﻿namespace ApiPublisher
{
    public interface IApiAccessor
    {
        void Post(string message);
    }
}
