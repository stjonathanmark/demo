﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiPublisher
{
    public class StarWarsApiAccessor : IApiAccessor
    {
        public StarWarsApiAccessor()
        {

        }

        public void Post(string message)
        {
            Console.WriteLine($"Posted to StarWars API: {message}");
        }
    }
}
