﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Unity;

namespace ApiPublisher
{
    public class Publisher
    {
        protected ICollection<IApiAccessor> apiAccessors;


        public Publisher()
        {
            apiAccessors = new List<IApiAccessor>();
            Intialize();
        }

        private void Intialize()
        {
            var apisSetting = ConfigurationManager.AppSettings["APIs"];

            if (string.IsNullOrEmpty(apisSetting)) throw new Exception("APIs setting is required.");

            var apis = apisSetting.Split(';');

            foreach (var api in apis)
            {
                IApiAccessor apiAccessor = IoC.Container.Resolve<IApiAccessor>(api);

                apiAccessors.Add(apiAccessor);
            }
        }

        public void Post(string message)
        {
            foreach (var apiAccessor in apiAccessors)
            {
                apiAccessor.Post(message);
                Console.WriteLine("");
            }

        }
    }
}
