﻿using System;

namespace ApiPublisher
{
    public class CpApiAccessor : IApiAccessor
    {
        public CpApiAccessor()
        {
        }

        public void Post(string message)
        {
            Console.WriteLine($"Posted to CP API: {message}");
        }
    }
}
