﻿namespace ApiPublisher
{
    public class ApiNames
    {
        public const string TDM = "TDM";
        public const string CP = "CP";
        public const string StarWars = "StarWars";
    }
}
