﻿using Unity;

namespace ApiPublisher
{
    public static class IoC
    {
        public static IUnityContainer Container = new UnityContainer();

        public static void ConfigureDependencies()
        {
            Container.RegisterType<IApiAccessor, TdmApiAccessor>(ApiNames.TDM);
            Container.RegisterType<IApiAccessor, CpApiAccessor>(ApiNames.CP);
            Container.RegisterType<IApiAccessor, StarWarsApiAccessor>(ApiNames.StarWars);
        }
    }
}
