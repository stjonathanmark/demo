﻿using System;

namespace ApiPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            IoC.ConfigureDependencies();

            Publisher publisher = new Publisher();

            publisher.Post("Some messsage");

            Console.ReadKey();
        }
    }
}
