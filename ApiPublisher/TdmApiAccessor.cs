﻿using System;

namespace ApiPublisher
{
    public class TdmApiAccessor : IApiAccessor
    {
        public TdmApiAccessor()
        {
        }

        public void Post(string message)
        {
            Console.WriteLine($"Posted to TDM API: {message}");
        }
    }
}
